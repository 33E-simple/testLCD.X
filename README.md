## Exercise LCD at various clock speeds

This simple applications runs LCD code (not using the LCD library) at
the following processor speeds:

* 70 MIPS (70.015)
* 60 MIPS (59.881)
* 50 MIPS (49.747)
* 40 MIPS (40.535)
* 35 MIPS (35.007)
