/*! \file  LCDpulseEnableBit.c
 *
 *  \brief Toggle the LCD enable bit
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:45 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "LCDinternal.h"

void delay_450_ns( void )
{
  int i,n;
#ifdef FCY
  #if FCY > 30000000
    n = 32;
  #else
    n = 16;
  #endif
#else
  n = 32;
#endif
  for (i=0; i<n; i++ )
    Nop();
}

//! Toggle the LCD enable bit
/*! Each LCD command is strobed into the device by raising the
 * enable bit for at least 40 microseconds.  This routine
 * provides this function to the other functions in the
 * library.
 *
 * Pseudocode:
 * \code
 * Wait a very short time
 * Set the LCD enable bit true
 * Wait a short time
 * Set the LCD enable bit false
 * Wait a very short time
 * \endcode
 *
 * \callergraph
 *
 * \param none
 * \return none
 */
void LCDpulseEnableBit( void )
{
  // Give data a chance to stabilize
  delay_450_ns();

  LCD_ENABLE = 1;

  // A little time to see the enable
  delay_450_ns();

  LCD_ENABLE = 0; // toggle E signal
  delay_450_ns();
}

