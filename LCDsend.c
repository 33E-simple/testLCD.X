/*! \file  LCDsend.c
 *
 *  \brief Send a byte to the LCD
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:47 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "LCDinternal.h"

//! LCDsend - Send a byte to the LCD
/*! Sends 8 bits of data to the LCD, pulses the LCD enable
 * bit to strobe the data in.
 *
 * Pseudocode:
 * \code
 * Shift the data 8 bits to the left and mask all except the top 4 bits
 * Clear the top 4 bits of the LCD data port
 * OR the masked data with the LCD data port
 * LCDpulseEnableBit()
 * Shift the data 2 bits to the left and mask all except the top 4 bits
 * Clear the top 4 bits of the LCD data port
 * OR the masked data with the LCD data port
 * LCDpulseEnableBit()
 * \endcode
 *
 * \callgraph
 * \callergraph
 * 
 * \param data char - byte to be sent
 * \return none
 */
void LCDsend( char cdata )
{
  unsigned int data,adata;

  data = (unsigned int) cdata;

  adata = ( data << 8 ) & 0xF000;
  LCD_DATA &= 0x0FFF;
  LCD_DATA |= adata;
  LCDpulseEnableBit();

  adata = ( data << 12 ) & 0xF000;
  LCD_DATA &= 0x0FFF;
  LCD_DATA |= adata;
  LCDpulseEnableBit();
}

