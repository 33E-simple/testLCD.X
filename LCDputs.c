/*! \file  LCDputs.c
 *
 *  \brief Send a string to the LCD
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:58 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "LCD.h"


//! Send a string to the LCD
/*! LCDputs() loops through each character of a null-terminated string,
 *  calling LCDletter() for each character.
 *
 * \callgraph
 *
 * \param p char * - pointer to string to be displayed
 * \return none
 */
void LCDputs( char *p )
{
    while (*p)
    {
        LCDletter(*p);
        p++;
    }
}
