/*! \file  testLCD.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date April 7, 2015, 3:16 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

// Tertiary programming pins
#pragma config ICS = PGD3
// Fast RC oscillator with PLL
#pragma config FNOSC = FRCPLL

// Postscaler
//#pragma config WDTPOST = PS32768
// Prescaler
//#pragma config WDTPRE = PR128
// Watchdog timer off
#pragma config FWDTEN = OFF
// Deadman timer off
#pragma config DMTEN = DISABLE

#include "LCD.h"

//#define DEFAULT_SPEED 45 // 43MIPS
//#define DEFAULT_SPEED 58 // 55MIPS
//#define DEFAULT_SPEED 63 // 60MIPS
#define DEFAULT_SPEED 74 // 70MIPS

int nDivs[5] = { 74, 42, 52, 63, 36 };
char szSpeeds[5][32] = {
                         "140 MHz 70 MIPS ",
                         " 81 MHz 40 MIPS ",
                         " 99 MHz 50 MIPS ",
                         "120 MHz 60 MIPS ",
                         " 70 MHz 35 MIPS " };
char szFOSC[8][8] = {
                    "FRC",
                    "FRCPLL",
                    "PRI",
                    "PRIPLL",
                    "BFRC",
                    "LPRC",
                    "FRCDIV16",
                    "FRCDIVN"
};

char szWork[32];

unsigned long lPLLPOST[4] = { 2,4,0,8 };
  int i,j,n;
  unsigned long speed1, speed2;
  unsigned int selection;

/*! Returns speed in KIPS */
unsigned long calculateSpeed( void )
{
  unsigned int n1, n2, n3;
  unsigned long ln1, ln2, ln3, speed;

  n1 = CLKDIVbits.PLLPRE;
  n2 = CLKDIVbits.PLLPOST;
  n3 = PLLFBD;
  ln3 = n3+2;
  ln2 = n1+2;
  ln1 = lPLLPOST[n2];
  speed = 7370 * ln3 / ln2 / ln1 / 2;
  return speed;
}

/*! main - */

/*!
 *
 */
int main( void )
{

  // Wait a while to prevent programmer from doing weird stuff
  Delay_ms(2000L*23L/70L);  // Should be close to 2 second

  // Check default speed
  speed1 = calculateSpeed();
  // and also read which oscillator
  selection = OSCCONbits.COSC;
  
  // Default clock
  CLKDIVbits.FRCDIV = 0;    // Divide by 1 = 8MHz
  CLKDIVbits.PLLPRE = 0;    // Divide by 2 = 4 MHz
  PLLFBD = DEFAULT_SPEED;   // Multiply by 58 = 232
  CLKDIVbits.PLLPOST = 0;   // Divide by 2 = 116
  speed2 = calculateSpeed();

  _TRISB6 = 0;
  
  _LATB6 = 1;
  LCDinit();
  _LATB6 = 0;

  LCDclear();

  sprintf(szWork,"Sel=%d %s         ",selection,szFOSC[selection]);
  LCDputs(szWork);
  Delay_ms(2000);
  for ( i=0; i<16; i++ )
    {
      LCDshiftLeft();
      Delay_ms(100);
    }
  LCDposition(0);
  sprintf(szWork,"Initial %7.3f               ",(double)speed1/1000.0);
  LCDputs(szWork);
  LCDposition(64);
  sprintf(szWork,"Default %7.3f                   ",(double)speed2/1000.0);
  LCDputs(szWork);
  for ( i=0; i<16; i++ )
    {
      LCDshiftRight();
      Delay_ms(100);
    }
  Delay_ms(5000);
  
  while(1)
    {
      for ( i=0; i<5; i++ )
        {
          PLLFBD = DEFAULT_SPEED;
          LCDclear();
          _LATB6 = 0;
          LCDputs("About to do     ");
          LCDposition(64);
          LCDputs(szSpeeds[i]);
          LCDposition(0);
          Delay_ms(2000);
          LCDputs("Doing          ");
          PLLFBD = nDivs[i];
          LCDposition(64);
//          LCDputs(szSpeeds[i]);
          sprintf(szWork,"%7.3f MIPS        ",(double)calculateSpeed()/1000.0);
          LCDputs(szWork);
          n = 6;
          //if ( i>3 )
          //  n=101;
          for ( j=0; j<n; j++ )
            {
              itoa(szWork,i,10);
              LCDposition(10);
              LCDputs(szWork);
              LCDputs("     ");
              itoa(szWork,j,10);
              LCDposition(13);
              LCDputs(szWork);
              _LATB6 = 1;
              Delay_ms(500);
              _LATB6 = 0;
              Delay_ms(500);
            }
        }
    }
  return 0;
}
